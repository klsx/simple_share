import random, string

SECRET_KEY = "".join([random.choice(string.printable) for _ in range(24)])

UPLOAD_DIR = ""

TOKEN_BYTES = 3

SALT = b'pout_salt_'

SEND_FILE_MAX_AGE_DEFAULT = 0

URL = "https://poutpout.ovh"