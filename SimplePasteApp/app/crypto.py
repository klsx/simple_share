import os, secrets, base64, io, hashlib

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet

from .views import app


def hashFilename(filename):
    filenameByte = str(filename).encode()
    hash = hashlib.sha256()
    return hash.hexdigest()


def genKey(filename):
    password_provided = str(filename)
    password = password_provided.encode()
    salt = app.config["SALT"]
    kdf = PBKDF2HMAC(
    algorithm=hashes.SHA256(),
    length=32,
    salt=salt,
    iterations=100000,
    backend=default_backend())
    key = base64.urlsafe_b64encode(kdf.derive(password))
    return key

def encrypt(inputFile, filename, filenameHashed):
    key = genKey(filename)

    fernet = Fernet(key)
    encrypted = fernet.encrypt(inputFile)

    with open(os.path.join(app.config['UPLOAD_DIR'], filenameHashed), 'wb') as f:
        f.write(encrypted)

def decode(inputFile, filename):
    key = genKey(filename)
    with open(inputFile, 'rb') as f:
        data = f.read()
    
    fernet = Fernet(key)
    decrypted = fernet.decrypt(data)

    return io.BytesIO(decrypted)