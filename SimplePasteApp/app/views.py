import os, secrets, urllib, io
from flask import Flask, flash, request, redirect, url_for, send_file, make_response
from werkzeug.utils import secure_filename

app = Flask(__name__)

app.config.from_object('config')

from .crypto import *


LINKS = {}

if app.config["UPLOAD_DIR"] == "":
    app.config["UPLOAD_DIR"] = os.path.join(app.root_path, 'upload')


@app.route('/', methods=['GET'])
def index():
    return ""

@app.route('/<file>', methods=['PUT'])
def put_file(file):
    maxopen = 1
    fileData = request.stream.read()
    filename = secure_filename(secrets.token_urlsafe(app.config["TOKEN_BYTES"]))
    filenameHashed = hashFilename(filename)
    encrypt(fileData, filename, filenameHashed)
    LINKS[filenameHashed] = {"file" : filenameHashed, "open": 0, "maxOpen": maxopen}
    url = "{}/".format(app.config['URL']) + filename + " "
    return url

@app.route('/', defaults={'maxopen': 1},methods=['POST'])
@app.route('/<int:maxopen>', methods=['POST'])
def upload_file(maxopen):
    if 'file' not in request.files:
        return "0"

    file = request.files['file']
    if file.filename == '':
        return "0"
    filename = secure_filename(secrets.token_urlsafe(app.config["TOKEN_BYTES"]))
    filenameHashed = hashFilename(filename)
    file_data = file.read()
    encrypt(file_data, filename, filenameHashed)
    LINKS[filenameHashed] = {"file" : filenameHashed, "open": 0, "maxOpen": maxopen}
    url = "{}/".format(app.config['URL']) + filename + " "
    return url


@app.route('/<string:token>', methods=['GET'])
def acces_file(token):
    hashedToken = hashFilename(token)
    if hashedToken in LINKS:
        if LINKS[hashedToken]["open"] < LINKS[hashedToken]["maxOpen"]:
            LINKS[hashedToken]["open"] +=1
            file_path = os.path.join(app.config['UPLOAD_DIR'], hashedToken)
            file_data = decode(file_path, token)
            response = make_response(send_file(file_data, mimetype='application/octet-stream'))

        if LINKS[hashedToken]["open"] == LINKS[hashedToken]["maxOpen"]:
            os.remove(os.path.join(app.config['UPLOAD_DIR'], hashedToken))
            LINKS.pop(hashedToken)
        return response
    else:
        return "This file doesn't exist"

if __name__ == "__main__":
    app.run()