FROM python:3.8-slim
RUN apt update -y && apt install -y nginx
RUN mkdir /app
COPY entrypoint.sh /app
WORKDIR /app
COPY ./SimplePasteApp /app
RUN pip3 install -r requirements.txt
RUN chmod +x ./entrypoint.sh
COPY ./SimplePasteApp/site.conf /etc/nginx/sites-available/
RUN ln -s /etc/nginx/sites-available/site.conf /etc/nginx/sites-enabled
ENTRYPOINT [ "sh", "entrypoint.sh"]